package pl.appivity.levelplugin;

import android.net.Uri;
import android.view.WindowManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.VideoView;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaWebView;
import org.json.JSONArray;
import org.json.JSONException;

import java.io.File;
import java.io.IOException;


public class AppivityLevelPlugin extends CordovaPlugin {
    private static final String TAG = "APPIVITY_LEVEL_PLUGIN";
    private static final String ACTION_PLAY = "PLAY";

    private VideoView video;

    @Override
    public void initialize(CordovaInterface cordova, CordovaWebView webView) {
        super.initialize(cordova, webView);
    }


    @Override
    public boolean execute(String action, JSONArray args, final CallbackContext callbackContext) throws JSONException {
        try {
            Log.d(TAG, "Action: " + action);

            if (AppivityLevelPlugin.ACTION_PLAY.equals(action)) {
            	return actionPlay(args, callbackContext);
            }
            
            callbackContext.error(TAG + ": INVALID ACTION");
            return false;
        } catch(Exception e) {
            Log.e(TAG, "ERROR: " + e.getMessage(), e);
            callbackContext.error(TAG + ": " + e.getMessage());
            return false;
        }
    }

    private boolean actionPlay(JSONArray args, final CallbackContext callbackContext) throws JSONException {
        cordova.getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                WebView thisWebView = (WebView)webView.getView();
                thisWebView.getSettings().setMediaPlaybackRequiresUserGesture(false);
                Log.d(TAG, "Settings changed");
           }
        });
        return true;
//        final RelativeLayout relativeLayout = new RelativeLayout(cordova.getActivity());
//
//        DisplayMetrics displaymetrics = new DisplayMetrics();
//        cordova.getActivity().getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
//        final int height = displaymetrics.heightPixels;
//        final int width = displaymetrics.widthPixels;
//
//        cordova.getActivity().runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
//                video = new VideoView(cordova.getActivity().getApplicationContext());
//                video.setLayoutParams(new FrameLayout.LayoutParams(width, height));
//                relativeLayout.addView(video);
////                video.setVideoURI(Uri.parse("file://" + cordova.getActivity().getFilesDir() +
////                        "/android_assets/www/plugins/pl" +
////                        ".appivity" +
////                        ".levelplugin/assets/levelforward.mp4"));
//                cordova.getActivity().addContentView(relativeLayout,
//                        new ViewGroup.LayoutParams(width, height));
//                ViewGroup parent = (ViewGroup) webView.getView().getParent();
//                parent.removeView(webView.getView());
//                webView.getView().setBackgroundColor(0x00000000);
//                RelativeLayout webViewRelative = new RelativeLayout(cordova.getActivity());
//                relativeLayout.addView(webView.getView(), new ViewGroup.LayoutParams(width, height));
//                cordova.getActivity().addContentView(webViewRelative, new ViewGroup.LayoutParams(width,
//                        height));
//
//                callbackContext.success(TAG + ": success" );
//            }
//        });
//    	return true;
    }


    //Plugin Method Overrides
    @Override
    public void onPause(boolean multitasking) {
        super.onPause(multitasking);
    }

    @Override
    public void onResume(boolean multitasking) {
        super.onResume(multitasking);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

}

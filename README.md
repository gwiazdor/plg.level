# Interfejs JS:

##Funkcja inicjalizująca plugin
#####wrapper (String): kontener w którym będzie osadzony plugin
#####startLevel (int): poziom początkowy (liczba całkowita z przedziału -100 - 100)
#####minLevel (int): margines dla minimalnego poziomu (od dołu ekranu)
#####maxLevel (int): margines dla maksymalnego poziomu (od góry ekranu)
```
init(wrapper, startLevel, minLevel, maxLevel)
```

##Funckja ustawiania poziomu
#####level (int): nowy poziom (liczba całkowita z przedziału -100 - 100)
```
setLevel(level)
```
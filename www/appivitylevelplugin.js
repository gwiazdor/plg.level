var cordova = require('cordova');

var appivitylevelplugin = {
    // stale
    PATH: '../plugins/pl.appivity.levelplugin/',
    RED_COLOR: '#FB0012',
    BLUE_COLOR: '#00A3D8',
    GREEN_COLOR: '#8FD800',
    VIDEO_ACT_FORWARD: 0,
    VIDEO_ACT_BACKWARD: 1,
    VIDEO_ACTION_LOOP: 0,
    VIDEO_ACTION_ROTATE: 1,
    ROTATE_CSS: 'rotate(180deg)',

    //zmienne
    videoReady: false,
    pluginLayer: undefined,
    pluginContentLayer: undefined,
    filterLayer: undefined,
    actLevel: 0,
    switchDelayTime: 100,
    viewHeight: 0,
    firstSetLevel: true,

    videoForward: undefined,
    videoBackward: undefined,
    videoDuration: 0,
    videoRealDuration: 0,
    timePerLevel: 0,
    videoLoopTime: 2,
    videoLoopMode: false,
    videoAct: undefined,
    videoRotated: false,

    videoListenerSetLoopAction: function(listener, time) { listener.set = true; listener.time = time;
        listener.action = 0; },
    videoListenerSetRotateAction: function(listener, time, timeAfterRotate) { listener.set = true; listener.time = time;
        listener.timeAfterRotate = timeAfterRotate; listener.action = 1; },
    videoListenerUnset: function(listener) { listener.set = false; listener.time = undefined;
        listener.timeAfterRotate = undefined; listener.action = undefined; },
    videoForwardListener: { id: 'forward', set: false, time: undefined, action: undefined, timeAfterRotate: undefined },
    videoBackwardListener: { id: 'backward', set: false, time: undefined, action: undefined,
        timeAfterRotate: undefined },

    //obsluga natywna dla Android
    ACTION_PLAY: 'PLAY',

    // pomocnicze
    getFilePath: function() {
        var args = Array.prototype.slice.call(arguments, 0);
        return this.PATH + args.join('/');
    },
    syncGet: function(url) {
        return $.ajax({type: 'GET', url: url, async: false, cache: false});
    },
    syncGetText: function(url) {
        return this.syncGet(url).responseText;
    },

    // interfejs do obslugi
    /**
     * Inicjalizacja pluginu
     * @param wrapper kontener dla pluginu
     * @param startLevel poziom początkowy
     * @param switchDelayTime czas przerwy między przełączaniem filmów
     */
    init: function(wrapper, startLevel, switchDelayTime) {
        console.log('Plugin init');
        wrapper = $(wrapper);
        var pluginLayer = this.syncGetText(this.getFilePath('assets', 'plugin_layer.html'));
        pluginLayer = $(pluginLayer);
        wrapper.append(pluginLayer);
        var filterLayer = pluginLayer.find('#filterLayer');
        var viewHeight = window.innerHeight;
        pluginLayer.height(viewHeight);
        filterLayer.height(viewHeight);
        this.pluginLayer = pluginLayer;
        this.pluginContentLayer = pluginLayer.find('#levelPluginContentLayer');
        this.filterLayer = filterLayer;
        this.viewHeight = viewHeight;
        if (switchDelayTime !== undefined) this.switchDelayTime = switchDelayTime;

        this.videoForward = pluginLayer.find('#levelForwardVideo')[0];
        $(this.videoForward).attr('src', this.getFilePath('assets', 'levelforward.mp4'));
        this.videoBackward = pluginLayer.find('#levelBackwardVideo')[0];
        $(this.videoBackward).attr('src', this.getFilePath('assets', 'levelbackward.mp4'));
        var _this = this;
        var createTimeUpdateListener = function(video1, video2, video1Listener, video2Listener) {
            return function() {
                //console.log(video1Listener, video2Listener);
                if (video1Listener.set == true &&
                        (video1Listener.time <= this.currentTime ||
                            (_this.videoDuration - this.currentTime <= 0.5
                            && _this.videoDuration - video1Listener.time <= 0.5)
                        )) {
                    video1.pause();
                    if (video1Listener.action == _this.VIDEO_ACTION_LOOP) {
                        console.log(video2Listener.id, video1.id, video2.id);
                        _this.loopVideo(video1, video2, video2Listener);
                        _this.videoListenerUnset(video1Listener);
                    } else if (video1Listener.action == _this.VIDEO_ACTION_ROTATE) {
                        // nie mogą być równe wartości bo iOS nie radzi sobie jak jest ustawione 0...
                        video2.currentTime = 0.1;
                        video1.currentTime = _this.videoDuration - 0.1;
                        var timeAfterRotate = video1Listener.timeAfterRotate;
                        setTimeout(function() {
                                if (_this.videoRotated == false) {
                                $(video1).css('transform', _this.ROTATE_CSS);
                                $(video2).css('transform', _this.ROTATE_CSS);
                                $(video1).css('-webkit-transform', _this.ROTATE_CSS);
                                $(video2).css('-webkit-transform', _this.ROTATE_CSS);
                                _this.videoRotated = true;
                            } else {
                                $(video1).css('transform', '');
                                $(video2).css('transform', '');
                                $(video1).css('-webkit-transform', '');
                                $(video2).css('-webkit-transform', '');
                                _this.videoRotated = false;
                            }
                            _this.replaceVideos(video2, video1);
                            console.log('Rotate action', video1Listener, timeAfterRotate);
                            _this.videoListenerSetLoopAction(video2Listener, timeAfterRotate);
                            video2.play();
                        }, _this.switchDelayTime);
                        _this.videoListenerUnset(video1Listener);
                    }
                }
            };
        };
        this.videoForward.addEventListener('timeupdate', createTimeUpdateListener(_this.videoForward,
            _this.videoBackward, _this.videoForwardListener, _this.videoBackwardListener));
        this.videoBackward.addEventListener('timeupdate', createTimeUpdateListener(_this.videoBackward,
            _this.videoForward, _this.videoBackwardListener, _this.videoForwardListener));
        this.videoForward.addEventListener('loadedmetadata', function() {
            _this.videoDuration = _this.videoForward.duration;
            _this.videoRealDuration = _this.videoDuration - _this.videoLoopTime;
            _this.timePerLevel = _this.videoRealDuration / 100.0;
            _this.videoReady = true;
            _this.setLevel(startLevel);
        });
    },
    /**
     * Ustawianie poziomu
     * @param level poziom (wartosc 0 - 100)
     */
    setLevel: function(level) {
        if (this.videoReady == false) return;
        if (level < -100 || level > 100) return;
        if (level == this.actLevel) return;

        var positive = false;
        if (level > 0) positive = true;
        var color = this.getGradientColor(Math.abs(level / 100.0), positive);
        var time = this.getTimeFromLevel(level);

        this.videoLoopMode = false;
        var _this = this;

        this.videoForward.pause();
        this.videoBackward.pause();
        if (this.videoAct == this.VIDEO_ACT_BACKWARD) {
            this.syncVideos(this.videoBackward, this.videoForward);
        } else {
            this.syncVideos(this.videoForward, this.videoBackward);
        }
        this.videoListenerUnset(this.videoBackwardListener);
        this.videoListenerUnset(this.videoForwardListener);

        var direction;
        if (this.videoRotated == false) {
            direction = level < this.actLevel;
        } else {
            direction = level > this.actLevel;
        }
        if (direction) {
            console.log('Level down', this.videoRotated);
            if (this.videoAct == undefined) this.videoAct = this.VIDEO_ACT_BACKWARD;
            if (_this.actLevel == 0 && _this.firstSetLevel) {
                _this.videoBackward.currentTime = _this.videoDuration - 0.5;
            }
            setTimeout(function() {
                _this.replaceVideos(_this.videoBackward, _this.videoForward);
                if ((_this.actLevel >= 0 && level < 0) || (_this.actLevel <= 0 && level > 0)) {
                    console.log('Set rotate action', time);
                    _this.videoBackward.play();
                    _this.setColor(color, _this.videoDuration + time);
                    _this.videoListenerSetRotateAction(_this.videoBackwardListener, _this.videoDuration, time);
                } else {
                    _this.videoBackward.play();
                    time = Math.abs(_this.videoDuration - time + _this.videoLoopTime);
                    _this.setColor(color, time);
                    _this.videoListenerSetLoopAction(_this.videoBackwardListener, time);
                }
                _this.actLevel = level;
            }, 100);
        } else {
            console.log('Level up', this.videoRotated);
            if (this.videoAct == undefined) this.videoAct = this.VIDEO_ACT_FORWARD;
            setTimeout(function() {
                _this.replaceVideos(_this.videoForward, _this.videoBackward);
                _this.videoForward.play();
                _this.actLevel = level;
                _this.setColor(color, time);
                _this.videoListenerSetLoopAction(_this.videoForwardListener, time);
            }, 100);
        }
        this.firstSetLevel = false;
    },

    // funkcje pomocnicze
    /**
     * Czy Android
     * @returns {boolean}
     */
    isAndroid: function() {
        return (navigator.userAgent.indexOf("Android") > 0);
    },
    generalSuccessCallback: function(data) { console.log(data); },
    generalErrorCallback: function(data) { console.log(data); },
    /**
     * Zwraca czas na podstawie poziomu
     * Ze względu na dokładność zdarzenia timeupdate - należy zaokrąglić do 0.25s
     * @param level poziom
     * @returns {number} czas
     */
    getTimeFromLevel: function(level) {
        level = Math.abs(level);
        var time = level * this.timePerLevel + this.videoLoopTime;
        if (time > this.videoDuration)
            return this.videoDuration;
        return time;
    },
    /**
     * Zapętalnie wideo
     * @param loopVideo wideo odtwarzane w przód
     * @param loopVideo2 wideo odtwarzane w tył
     */
    loopVideo: function(loopVideo, loopVideo2, video2Listener) {
        var _this = this;
        this.videoLoopMode = true;
        var switchVideos = function(video, video2) {
            if (_this.videoLoopMode == false) return;
            video.pause();
            video2.pause();
            _this.syncVideos(video, video2);
            // trzeba delikatnie opóźnić podmianę filmów - inaczej jest brzydki efekt przejścia
            setTimeout(function() {
                _this.replaceVideos(video2, video);
                video2.play();
                if (_this.videoLoopMode)
                    _this.videoListenerSetLoopAction(video2Listener, video2.currentTime + _this.videoLoopTime);
            }, _this.switchDelayTime);
        };
        switchVideos(loopVideo, loopVideo2);
    },
    /**
     * Synchronizacja czasowa filmów
     * @param video film do którego będzie synchronizowany czas
     * @param video2 film który zostanie zsynchronizowany
     */
    syncVideos: function(video, video2) {
        video2.currentTime = Math.abs(this.videoDuration - video.currentTime);
    },
    /**
     * Zmiana położenia
     * @param video wideo z przodu
     * @param video2 wideo z tyłu
     */
    replaceVideos: function(video, video2) {
        $(video).css('z-index', 2);
        $(video2).css('z-index', 1);
        if (video.id == 'levelForwardVideo') {
            this.videoAct = this.VIDEO_ACT_FORWARD;
        } else {
            this.videoAct = this.VIDEO_ACT_BACKWARD;
        }
    },
    /**
     *
     * @param color
     * @param time
     */
    setColor: function(color, time) {
        this.filterLayer.css('-webkit-transition-duration', time + 's');
        this.filterLayer.css('transition-duration', time + 's');
        this.filterLayer.css('background-color', color);
    },
    rotateMovies: function() {
        console.log('Rotating movies', this);
        if (this.videoRotated == false) {
            this.videoRotated = true;
            $(this.videoForward).css('transform', 'rotate(180deg)');
            $(this.videoBackward).css('transform', 'rotate(180deg)');
        } else {
            this.videoRotated = false;
            $(this.videoForward).css('transform', '');
            $(this.videoBackward).css('transform', '');
        }
    },
    getGradientColor: function (percent, positive) {
        var start_color = this.BLUE_COLOR;
        var end_color = this.RED_COLOR;
        if (positive == true) {
            start_color = this.BLUE_COLOR;
            end_color = this.GREEN_COLOR;
        }

        // strip the leading # if it's there
        start_color = start_color.replace(/^\s*#|\s*$/g, '');
        end_color = end_color.replace(/^\s*#|\s*$/g, '');

        // convert 3 char codes --> 6, e.g. `E0F` --> `EE00FF`
        if (start_color.length == 3) {
            start_color = start_color.replace(/(.)/g, '$1$1');
        }

        if (end_color.length == 3) {
            end_color = end_color.replace(/(.)/g, '$1$1');
        }

        // get colors
        var start_red = parseInt(start_color.substr(0, 2), 16),
            start_green = parseInt(start_color.substr(2, 2), 16),
            start_blue = parseInt(start_color.substr(4, 2), 16);

        var end_red = parseInt(end_color.substr(0, 2), 16),
            end_green = parseInt(end_color.substr(2, 2), 16),
            end_blue = parseInt(end_color.substr(4, 2), 16);

        // calculate new color
        var diff_red = end_red - start_red;
        var diff_green = end_green - start_green;
        var diff_blue = end_blue - start_blue;

        diff_red = ( (diff_red * percent) + start_red ).toString(16).split('.')[0];
        diff_green = ( (diff_green * percent) + start_green ).toString(16).split('.')[0];
        diff_blue = ( (diff_blue * percent) + start_blue ).toString(16).split('.')[0];

        // ensure 2 digits by color
        if (diff_red.length == 1)
            diff_red = '0' + diff_red;

        if (diff_green.length == 1)
            diff_green = '0' + diff_green;

        if (diff_blue.length == 1)
            diff_blue = '0' + diff_blue;

        return '#' + diff_red + diff_green + diff_blue;
    }
};

module.exports = appivitylevelplugin;
window.Plugin.appivityfotovideoplugin = appivitylevelplugin;